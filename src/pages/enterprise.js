import React , {Component} from 'react';
import { View, Text, StyleSheet, AsyncStorage} from 'react-native';

export default class Enterprise extends Component {
    static navigationOptions = {
		title: "Enterprise"
    };

    constructor(props) {
        super(props);
        this.state = {
            enterprise: {}
        }
    }

    componentDidMount(){        
        let id = this.props.navigation.getParam('id');
        AsyncStorage.getItem('APIKeys').then(APIKeys => {
            if (!APIKeys) return;
            APIKeys = JSON.parse(APIKeys);
            fetch(`http://empresas.ioasys.com.br/api/v1/enterprises/${id}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'access-token': APIKeys.accessToken,
                    'client': APIKeys.client,
                    'uid': APIKeys.uid
                },
            }).then((response) => response.json()).then((responseJson) => {
                this.setState({ enterprise: responseJson.enterprise });
            }).catch(err => console.log(err))
        }).catch(err => console.log(err));
    } 
    
    render() {
		return (
            <View style={styles.item}>
                { !!this.state.enterprise.enterprise_name  && <Text style={styles.title}>{ this.state.enterprise.enterprise_name }</Text>}
                { !!this.state.enterprise.email_enterprise  && <Text style={styles.desc}>E-mail: { this.state.enterprise.email_enterprise }</Text>}
                { !!this.state.enterprise.facebook  && <Text style={styles.desc}>Facebook: { this.state.enterprise.facebook }</Text>}
                { !!this.state.enterprise.twitter  && <Text style={styles.desc}>Twitter: { this.state.enterprise.twitter }</Text>}
                { !!this.state.enterprise.linkedin  && <Text style={styles.desc}>Linkedin: { this.state.enterprise.linkedin }</Text>}
                { !!this.state.enterprise.phone  && <Text style={styles.desc}>Phone: { this.state.enterprise.phone }</Text>}
                { !!this.state.enterprise.share_price  && <Text style={styles.desc}>Share price: { this.state.enterprise.share_price }</Text>}
                { !!this.state.enterprise.city  && <Text style={styles.desc}>City: { this.state.enterprise.city }</Text>}
                { !!this.state.enterprise.country  && <Text style={styles.desc}>Country: { this.state.enterprise.country }</Text>}                    
                { !!this.state.enterprise.description  && <Text style={styles.desc}>{ this.state.enterprise.description }</Text>}
            </View>
        );
    }
}

const styles = StyleSheet.create({
	item: {
        backgroundColor: "#FFF",
        borderWidth: 1,
        borderColor: "#DDD",
        borderRadius: 5,
        padding: 20,
        marginBottom: 20
    },
    title: {
        fontSize: 18,
        fontWeight: "bold",
        color: "#333"
    },
    desc: {
        fontSize: 16,
        color: "#999",
        marginTop: 5,
        lineHeight: 24
    },
});