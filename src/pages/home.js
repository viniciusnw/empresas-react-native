import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    AsyncStorage
} from 'react-native';

import EnterpriseList from './../components/EnterpriseList';
export default class Main extends Component {
    static navigationOptions = {
		title: "Home"
	};

    constructor(props) {
        super(props);
        this.state = {
            enterprises: []
        }
        this.enterpriseNavigation = this.enterpriseNavigation.bind(this);
    }

    componentDidMount() {
        fetch('http://empresas.ioasys.com.br/api/v1/users/auth/sign_in', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: "testeapple@ioasys.com.br",
                password: "12341234"
            })
        }).then((response) => {
            AsyncStorage.setItem('APIKeys', JSON.stringify({
                uid: response.headers.get('uid'),
                client: response.headers.get('client'),
                accessToken: response.headers.get('access-token')
            })).then(() => {
                console.log('Save api keys');
                this.getEnterprises();
            });
        }).catch((error) => {
            console.error(error);
        });
    }

    getEnterprises() {
        AsyncStorage.getItem('APIKeys').then(APIKeys => {
            if (!APIKeys) return;
            APIKeys = JSON.parse(APIKeys);
            fetch('http://empresas.ioasys.com.br/api/v1/enterprises', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'access-token': APIKeys.accessToken,
                    'client': APIKeys.client,
                    'uid': APIKeys.uid
                },
            }).then((response) => response.json()).then((responseJson) => {
                this.setState({ enterprises: responseJson.enterprises });
            }).catch(err => console.log(err))
        }).catch(err => console.log(err));
    }

    enterpriseNavigation(id){
        this.props.navigation.navigate("Enterprise", {id: id});
    }

    render() {
        return (
            <View style={styles.container}>
                <EnterpriseList enterpriseNavigation={this.enterpriseNavigation} list={this.state.enterprises}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#F5FCFF',
        padding: 20,
        paddingTop: 10
    }
});