import {
    createStackNavigator
} from 'react-navigation';

import Home from "./pages/home";
import Enterprise from "./pages/enterprise";

export default createStackNavigator({
    Home,
    Enterprise
}, {
    navigationOptions: {
        headerStyle: {
            backgroundColor: "#007bff"
        },
        headerTintColor: "#FFF"
    },
});