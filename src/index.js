import React from 'react';
import Routes from './routes';
import "./config/statusBar";

const APP = () => <Routes />;

export default APP;