import React, { Component } from 'react';
import {
    FlatList,
    StyleSheet
} from 'react-native';
import EnterpriseItem from './EnterpriseItem';

export default class EnterpriseList extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <FlatList
                keyExtractor={item => item.id.toString()}
                data={this.props.list}
                renderItem={({ item }) => <EnterpriseItem enterpriseNavigation={this.props.enterpriseNavigation} item={item} />} />
        );
    }
}