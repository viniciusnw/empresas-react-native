import React, { Component } from 'react';
import {
    View,
    Image,
    Text,
    TouchableOpacity,
    StyleSheet
} from 'react-native';

export default class EnterpriseItem extends Component {

    constructor(props) {
        super(props);        
    }

    render() {
        let pic = {
            uri: (this.props.item.photo) 
            ? `http://empresas.ioasys.com.br${this.props.item.photo}` 
            : 'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?f=y'
        };
        return (
            <View style={styles.item}>
                <View style={styles.header}>
                    <Image source={pic} style={styles.image} />
                    <Text style={styles.title}>{this.props.item.enterprise_name}</Text>
                </View>
                <Text style={styles.desc}>Share price: {this.props.item.share_price}</Text>
                <Text style={styles.desc}>City: {this.props.item.city}</Text>
                <Text style={styles.desc}>Country: {this.props.item.country}</Text>

                <TouchableOpacity style={styles.button}
                    onPress={() => this.props.enterpriseNavigation(this.props.item.id)}>
                    <Text style={styles.buttonText}>Acessar</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    item: {
        backgroundColor: "#FFF",
        borderWidth: 1,
        borderColor: "#DDD",
        borderRadius: 5,
        padding: 20,
        marginBottom: 20
    },
    header: {
        margin: 10,
        marginLeft: 0,
        flexDirection: 'row',
        alignItems: 'center'
    },
    image: {
        margin: 10,
        marginTop: 0,
        marginLeft: 0,
        borderRadius: 20,
        width: 40, height: 40
    },
    title: {
        fontSize: 18,
        fontWeight: "bold",
        color: "#333"
    },
    desc: {
        fontSize: 16,
        color: "#999",
        marginTop: 5,
        lineHeight: 24
    },
    button: {
        height: 42,
        borderRadius: 5,
        borderWidth: 2,
        borderColor: "#007bff",
        backgroundColor: "transparent",
        justifyContent: "center",
        alignItems: "center",
        marginTop: 15
    },
    buttonText: {
        fontSize: 16,
        color: "#007bff",
        fontWeight: "bold"
    }
});